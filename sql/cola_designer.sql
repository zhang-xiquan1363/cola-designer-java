/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : cola_designer

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 09/04/2022 23:24:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for design_data
-- ----------------------------
DROP TABLE IF EXISTS `design_data`;
CREATE TABLE `design_data`  (
                                `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                                `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                                `simple_desc` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                                `bg_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                                `bg_color` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                                `scale_x` int(11) NULL DEFAULT NULL COMMENT '屏幕比例X',
                                `scale_y` int(11) NULL DEFAULT NULL COMMENT '屏幕比例Y',
                                `components` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '页面组件',
                                `design_img_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设计预览图',
                                `state` tinyint(4) NULL DEFAULT 1 COMMENT '禁用状态：1启用,-1禁用',
                                `view_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问码',
                                `count_view` int(11) NULL DEFAULT NULL COMMENT '访问量',
                                `create_user` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
                                `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of design_data
-- ----------------------------
INSERT INTO `design_data` VALUES ('e6bc80a77e178f16dda517a60a700aac', 'niub', NULL, NULL, '#2B3340', 1920, 1080, '', '', 1, NULL, NULL, NULL, '2022-04-08 20:51:51', '2022-04-09 23:24:22');

-- ----------------------------
-- Table structure for design_img_group
-- ----------------------------
DROP TABLE IF EXISTS `design_img_group`;
CREATE TABLE `design_img_group`  (
                                     `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                                     `group_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                                     `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of design_img_group
-- ----------------------------
INSERT INTO `design_img_group` VALUES ('1456457428993859585', '背景库', '2021-11-05 11:04:45');
INSERT INTO `design_img_group` VALUES ('1459092876190928897', '图片库', '2021-11-12 17:37:04');
INSERT INTO `design_img_group` VALUES ('1459092992377344002', '素材库', '2021-11-12 17:37:32');

-- ----------------------------
-- Table structure for design_img_pool
-- ----------------------------
DROP TABLE IF EXISTS `design_img_pool`;
CREATE TABLE `design_img_pool`  (
                                    `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                                    `img_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片名',
                                    `file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                                    `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组',
                                    `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
                                    `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
                                    `count_star` int(11) NULL DEFAULT NULL COMMENT '点赞量',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of design_img_pool
-- ----------------------------
INSERT INTO `design_img_pool` VALUES ('1459093044940361730', 'QQ截图20200910154632', 'b9886fc42ca51ef5d800aecf8ce1cd0b', '1459092876190928897', '2021-11-12 17:37:45', '1001', NULL);

-- ----------------------------
-- Table structure for sys_access
-- ----------------------------
DROP TABLE IF EXISTS `sys_access`;
CREATE TABLE `sys_access`  (
                               `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                               `access_name` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名字',
                               `access_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限的规则',
                               `parent_id` int(11) NULL DEFAULT NULL COMMENT '权限父级id access_parent',
                               `menu_level` int(11) NULL DEFAULT NULL COMMENT '菜单等级(0:方法；1:一级；2:2级；)',
                               `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前端组件',
                               `menu_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
                               `sort_num` int(11) NULL DEFAULT NULL COMMENT '排序值',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_access
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
                             `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                             `dict_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `dict_tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `dict_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `dict_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `create_time` datetime(0) NULL DEFAULT NULL,
                             `update_time` datetime(0) NULL DEFAULT NULL,
                             `update_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名',
                             `role_desc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
                             `create_time` datetime(0) NULL DEFAULT NULL,
                             `update_time` datetime(0) NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', '上帝视角6', '2021-07-16 10:59:35', '2021-07-16 10:59:36');

-- ----------------------------
-- Table structure for sys_role_access
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_access`;
CREATE TABLE `sys_role_access`  (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `role_id` int(11) NOT NULL COMMENT '角色ID',
                                    `access_id` int(11) NULL DEFAULT NULL COMMENT '权限ID',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_access
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                             `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `enabled` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `role_id` tinyint(4) NULL DEFAULT NULL,
                             `create_time` datetime(0) NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1001', 'admin', '$2a$10$MxxYsl3zANgsSCxJpsQazeVhA/K4H23w2A/ZCLz8LQM/qogdCa6vy', '18123416815', 'colaiven@aliyun.com', '1', 1, '2019-11-14 11:52:22');

SET FOREIGN_KEY_CHECKS = 1;
